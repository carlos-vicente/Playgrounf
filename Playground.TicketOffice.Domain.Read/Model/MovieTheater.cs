﻿namespace Playground.TicketOffice.Domain.Read.Model
{
    public class MovieTheater
    {
        public string Name { get; set; }

        public int RoomsNumber { get; set; } 
    }
}