﻿using System;

namespace Playground.Domain.Persistence.PostgreSQL.Queries
{
    public class CheckIfStreamExistsQuery
    {
        public Guid streamId { get; set; } 
    }
}