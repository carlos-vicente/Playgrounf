﻿using System.Web.Mvc;

namespace Playground.TicketOffice.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}